{-# LANGUAGE OverloadedLists #-}

module Language.Haskell.TH.FreeVariables
  ( freeVars
  ) where

import           Data.Maybe
import           Data.Semigroup                 ( Semigroup(..) )
import           Data.Set                       ( Set )
import qualified Data.Set                      as S
import           Language.Haskell.TH.Syntax    as H

-- Returns the free variables appearing in an expression. This doesn't
-- include constructor names or record labels (unless they appear as
-- functions).
freeVars :: H.Exp -> Set H.Name
freeVars (H.VarE name       ) = [name]
freeVars (H.ConE _          ) = []
freeVars (H.LitE _          ) = []
freeVars (H.AppE     e1 e2  ) = freeVars e1 <> freeVars e2
freeVars (H.AppTypeE e  _   ) = freeVars e
freeVars (H.InfixE  e1 e2 e3) = foldMap freeVars $ catMaybes [e1, Just e2, e3]
freeVars (H.UInfixE e1 e2 e3) = foldMap freeVars ([e1, e2, e3] :: [H.Exp])
freeVars (H.ParensE e       ) = freeVars e
freeVars (H.LamE ps e       ) = (freeVars e S.\\ boundInPs) <> usedInPs
  where VarsP { boundVars = boundInPs, usedVars = usedInPs } = varsInPats ps
freeVars (H.LamCaseE    ms      ) = foldMap freeVarsMatch ms
freeVars (H.TupE        es      ) = foldMap (foldMap freeVars) es
freeVars (H.UnboxedTupE es      ) = foldMap (foldMap freeVars) es
freeVars (H.UnboxedSumE e  _  _ ) = freeVars e
freeVars (H.CondE       e1 e2 e3) = foldMap freeVars ([e1, e2, e3] :: [H.Exp])
freeVars (H.MultiIfE brs        ) = foldMap varsInBranch brs
freeVars (H.LetE ds exp         ) = (freeVars exp <> usedInDs) S.\\ boundInDs
 where
  VarsP { boundVars = boundInDs, usedVars = usedInDs } = foldMap varsInDec ds
freeVars (H.CaseE e ms) = freeVars e <> foldMap freeVarsMatch ms
freeVars (H.DoE  ss   ) = usedVars (varsInStmts ss)
freeVars (H.MDoE ss   ) = usedInS S.\\ boundsInS -- Remove *all* variables bound in ss because of recursive do
  where VarsP { boundVars = boundsInS, usedVars = usedInS } = varsInStmts ss
freeVars (H.CompE     ss ) = usedVars (varsInStmts ss)
freeVars (H.ArithSeqE rng) = case rng of
  FromR exp            -> freeVars exp
  FromThenR e1 e2      -> freeVars e1 <> freeVars e2
  FromToR   e1 e2      -> freeVars e1 <> freeVars e2
  FromThenToR e1 e2 e3 -> freeVars e1 <> freeVars e2 <> freeVars e3
freeVars (H.ListE es           ) = foldMap freeVars es
freeVars (H.SigE    e _        ) = freeVars e
freeVars (H.RecConE _ fes      ) = foldMap (freeVars . snd) fes
freeVars (H.RecUpdE e fes      ) = freeVars e <> foldMap (freeVars . snd) fes
freeVars (H.StaticE           e) = freeVars e
freeVars (H.UnboundVarE       n) = [n] -- Am I supposed to do this?
freeVars (H.LabelE            _) = []
freeVars (H.ImplicitParamVarE _) = []

-- Returns the variables bound by a construction and the variables it
-- uses.
data VarsP = VarsP
  { boundVars :: Set H.Name
  , usedVars  :: Set H.Name
  }

instance Semigroup VarsP where
  VarsP p1 q1 <> VarsP p2 q2 = VarsP (p1 <> p2) (q1 <> q2)

instance Monoid VarsP where
  mempty  = VarsP [] []
  mappend = (<>)

varsInP :: H.Pat -> VarsP
varsInP (H.LitP        _    ) = mempty
varsInP (H.VarP        v    ) = mempty { boundVars = [v] }
varsInP (H.TupP        ps   ) = varsInPats ps
varsInP (H.UnboxedTupP ps   ) = varsInPats ps
varsInP (H.UnboxedSumP p _ _) = varsInP p
varsInP (H.ConP _ es        ) = varsInPats es
varsInP (H.InfixP  p _ p'   ) = varsInPats [p, p']
varsInP (H.UInfixP p _ p'   ) = varsInPats [p, p']
varsInP (H.ParensP p        ) = varsInP p
varsInP (H.TildeP  p        ) = varsInP p
varsInP (H.BangP   p        ) = varsInP p
varsInP (H.AsP n p          ) = mempty { boundVars = [n] } <> varsInP p
varsInP H.WildP               = mempty
varsInP (H.RecP _ fs)         = varsInPats (fmap snd fs)
varsInP (H.ListP ps )         = varsInPats ps
varsInP (H.SigP  p _)         = varsInP p
varsInP (H.ViewP e p)         = mempty { usedVars = freeVars e } <> varsInP p

varsInBranch :: (H.Guard, H.Exp) -> Set H.Name
varsInBranch (guard, exp) = (freeVars exp S.\\ boundInG) <> usedInG
  where VarsP { boundVars = boundInG, usedVars = usedInG } = varsInGuard guard

-- Variables appearing in a list of patterns. We need to be careful
-- because a variable bound in a left pattern can be used in a view
-- pattern on the right and thus must not be considered a used
-- variable.
varsInPats :: [H.Pat] -> VarsP
varsInPats = foldr go mempty
 where
  go pat VarsP { boundVars = boundRight, usedVars = usedRight } = VarsP
    { boundVars = bound <> boundRight
    , usedVars  = usedRight S.\\ bound
    }
    where VarsP { boundVars = bound, usedVars = used } = varsInP pat

varsInBody :: H.Body -> Set H.Name
varsInBody (H.NormalB  exp) = freeVars exp
varsInBody (H.GuardedB gds) = foldMap varsInBranch gds

freeVarsMatch :: H.Match -> Set H.Name
freeVarsMatch (H.Match pat body decs) =
  (varsInBody body <> usedInP <> usedInD) S.\\ (boundInP <> boundInD)
 where
  VarsP { boundVars = boundInP, usedVars = usedInP } = varsInP pat

  VarsP { boundVars = boundInD, usedVars = usedInD } = foldMap varsInDec decs

varsInGuard :: H.Guard -> VarsP
varsInGuard (H.NormalG guard) = mempty { usedVars = freeVars guard }
varsInGuard (H.PatG    stmts) = varsInStmts stmts

varsInStmt :: H.Stmt -> VarsP
varsInStmt (H.BindS pat exp) =
  varsInP pat <> mempty { usedVars = freeVars exp }
varsInStmt (H.LetS    ds ) = foldMap varsInDec ds
varsInStmt (H.NoBindS exp) = mempty { usedVars = freeVars exp }
varsInStmt (H.ParS    ps ) = foldMap (foldMap varsInStmt) ps
varsInStmt (H.RecS    ss ) = VarsP { boundVars = boundInS
                                   , usedVars  = usedInS S.\\ boundInS
                                   }
  where VarsP { boundVars = boundInS, usedVars = usedInS } = varsInStmts ss

varsInStmts :: [H.Stmt] -> VarsP
varsInStmts = foldr go mempty
 where
  go stmt VarsP { boundVars = boundAfter, usedVars = usedAfter } = VarsP
    { boundVars = boundNow <> boundAfter
    , usedVars  = (usedAfter S.\\ boundNow) <> usedNow
    }
    where VarsP { boundVars = boundNow, usedVars = usedNow } = varsInStmt stmt

varsInClause :: H.Clause -> VarsP
varsInClause (H.Clause ps body ds) = VarsP
  { boundVars = boundInPs
  , usedVars  = usedInPs
                  <> ((varsInBody body <> usedInDs) S.\\ (boundInPs <> boundInDs)
                     )
  }
 where
  VarsP { boundVars = boundInPs, usedVars = usedInPs } = varsInPats ps

  VarsP { boundVars = boundInDs, usedVars = usedInDs } = varsInDecs ds

varsInDec :: H.Dec -> VarsP
varsInDec (H.FunD fun clauses) = VarsP
  { boundVars = [fun]
  , usedVars  = S.delete fun (usedVars clausesVars)
  }
  where clausesVars = foldMap varsInClause clauses
varsInDec (H.ValD pat body decs) = VarsP
  { boundVars = boundInP
  , usedVars  = ((varsInBody body <> usedInDecs) S.\\ boundInP) <> usedInP
  }
 where
  VarsP { usedVars = usedInDecs }                    = varsInDecs decs
  VarsP { boundVars = boundInP, usedVars = usedInP } = varsInP pat
varsInDec _ = mempty

varsInDecs :: [H.Dec] -> VarsP
varsInDecs = foldMap varsInDec
